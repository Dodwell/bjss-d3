//Width and height
var w = 1400;
var h = 350;
const numDaysInSprint = 10;

var colourBySize = true;

var data;


var dataset = [ {cycleTime:5, key:"1"}, 
{cycleTime:10, key:"2"},
{cycleTime:13, key:"3"},
{cycleTime:19, key:"4"}, 
{cycleTime:21, key:"5"}, 
{cycleTime:15,key:"6"},
{cycleTime:22, key:"7"}, 
{cycleTime:18, key:"8"}, 
{cycleTime:15, key:"9"},
{cycleTime:13, key:"10"},
{cycleTime:6, key:"test small"},
{cycleTime:28, key:"test large"}];

//Create SVG element
var svg = d3.select("#chart")
			.append("svg")
			.attr("width", w)
			.attr("height", h);

const margin = {top:20, right:20, bottom:50, left:70};
const graphWidth = w - margin.left - margin.right;
const graphHeight = h - margin.top - margin.bottom;

const graph = svg.append('g')
	.attr('width', graphWidth)
	.attr('height', graphHeight)
	.attr('transform', `translate(${margin.left},${margin.top})`);

const xAxisGroup = graph.append('g')
	.attr('transform', `translate(0, ${graphHeight})`);

const yAxisGroup = graph.append('g');	

xAxisGroup.selectAll('text')
  .attr('fill', 'orange')
  .attr('transform', 'rotate(-40)')
  .attr('text-anchor', 'end');

var xScale = d3.scaleBand()
  .range([0, graphWidth])
  .paddingInner(0.2)
  .paddingOuter(0.2);

var yScale = d3.scaleLinear()
  .range([graphHeight, 0]);


// the update function
const update = () => {

	// 1. update scales (input domains) if they rely on the data
	xScale.domain(d3.range(data.length));
	yScale.domain([0, d3.max(data, d => d.cycleTime)	]);

	// 2. join updated data to elements
	var rects = graph.selectAll("rect")
		.data(data);

	// 3. remove (any) unwanted elements using the exit selection
	rects.exit().remove();

	// 4. update the current shapes in the dom (from the data)
	console.log(d3.range(data.length));
	console.log(xScale(3));
	console.log(xScale.bandwidth());
		
	rects.attr("x", function(d, i) {
				return xScale(i);
		})
		.attr("y", 		d => yScale(d.cycleTime))
		.attr("width", 	xScale.bandwidth())
		.attr("height", d => graphHeight - yScale(d.cycleTime))
		.attr("fill", 	d=> setColour(d));

	// 5. append the enter selection to the dom			

	rects.enter()
		.append("rect")
		.attr("x", function(d, i) {
				return xScale(i);
		})
		.attr("y", function(d) {
			//console.log(d[0]);
			return yScale(d.cycleTime);
		})
		.attr("width", 	xScale.bandwidth())
		.attr("height", d => graphHeight - yScale(d.cycleTime))
		.attr("fill", 	d => setColour(d) )
		.on("mouseover", function(d) {

			//Get this bar's x/y values, then augment for the tooltip
			var xPosition = parseFloat(d3.select(this).attr("x")) + xScale.bandwidth() / 2;
			var yPosition = parseFloat(d3.select(this).attr("y")) / 2 + h / 2;

			var arrString=["test1", "test2", "test3"];
			//Update the tooltip position and value
			d3.select("#tooltip")
				.style("left", xPosition + "px")
				.style("top", yPosition + "px")						
				.select("#toolTipText")
				.text("ID: "+d.key)
				.append('br')
				.append('span')
				.text("Sprint: "+d.doneSprintName);
			d3.select("#tooltip")
				.select("#value")
				.text("Cycle Time: "+d.cycleTime)
				.append('br');
				
			d3.select("#tooltip")
				.select("#value")
				.selectAll('span')
				.data(d.statuses)
				.enter()				
				.append('span')
				.text(function(d){
					var strStatus = d.type + ' - ' + d.fromStr +' - '+ d.toStr;
					if (d.diff > 0) {
						strStatus += ' ('+d.diff+')';
					}
				  return strStatus;
				})
				.append('br');
					
		
				//Show the tooltip
				d3.select("#tooltip").classed("hidden", false);

		})
		.on("mouseout", function() {		
				//Hide the tooltip
				d3.select("#tooltip").classed("hidden", true);
		})
		.on("click", function() {
				sortBars(xScale);
		});

	const xAxis = d3.axisBottom(xScale)
		.tickValues(xScale.domain().filter(function(d,i){ return !(i%10)})); 
	const yAxis = d3.axisLeft(yScale);

	xAxisGroup.call(xAxis);
	yAxisGroup.call(yAxis);

}

//Define sort order flag
var sortOrder = false;

//Define sort function
var sortBars = function(xScale) {

	//Flip value of sortOrder
	sortOrder = !sortOrder;

	graph.selectAll("rect")
	   .sort(function(a, b) {
			if (sortOrder) {
				return d3.ascending(a, b);
			} else {
				return d3.descending(a, b);
			}
		})
	   .transition()
	   .delay(function(d, i) {
		   return i * 50;
	   })
	   .duration(1000)
	   .attr("x", function(d, i) {
			return xScale(i);
	   });

};	

function setColour(d) {
	if (colourBySize) {
		var colourShade = Math.round(d.cycleTime * 10)
		var strRGB;
		if (d.cycleTime < numDaysInSprint) {
			strRGB = "rgb(0, 200, 0)";
		} else {
			strRGB = "rgb(0, 0, " + colourShade + ")";
		}
	} else {
		switch(d.type) {
			case "Story":
				strRGB = "rgb(0, 200, 0)";
				break;
			case "Bug":
				strRGB = "rgb(200, 0, 0)";
				break;
			case "Tech Task":
				strRGB = "rgb(0, 0, 200)";
				break;
			case "Spike":
				strRGB = "rgb(100, 000, 0)";
				break;
			default:
				strRGB = "rgb(200, 0, 200)";
		}
	}
	return strRGB;
}

function doRecolour() {
	// toggle colour
	colourBySize = !colourBySize;
	
	update();
	
	
}

function doRegroup() {
	var nested_data = d3.nest()
		.key(function(d) { return d.status; })
		.key(function(d) { return d.priority; })
	
}

function doRefresh() {
	var url="http://localhost:3000/team/Axe/stories";
	//var url="../sid-stories.json";
	d3.json(url, function(error, dataset) {
		console.log(dataset);

		if (error) {
			throw error;
		}
		var storiesBySprint = d3.nest()
		  .key(function(d) { return d.doneSprintName; })
		  .entries(dataset);

		console.log(storiesBySprint);
		
		data = dataset;
		update();
	});
}

doRefresh();

function gridData() {
	var data = new Array();
	var xpos = 1; //starting xpos and ypos at 1 so the stroke will show when we make the grid below
	var ypos = 1;
	var width = 50;
	var height = 50;
	var click = 0;
	
	// iterate for rows	
	for (var row = 0; row < 5; row++) {
		data.push( new Array() );
		
		// iterate for cells/columns inside rows
		for (var column = 0; column < 10; column++) {
			data[row].push({
				x: xpos,
				y: ypos,
				width: width,
				height: height,
				click: column%4			
			})
			// increment the x position. I.e. move it over by 50 (width variable)
			xpos += width;
		}
		// reset the x position after a row is complete
		xpos = 1;
		// increment the y position for the next row. Move it down 50 (height variable)
		ypos += height;	
	}
	return data;
}

function getColour(index) {
	var arrColours = ["#fff", "#2C93E8", "#F56C4E", "#838690"];
	return arrColours[index%4];
		
}


var gridData = gridData();	
// I like to log the data to the console for quick debugging
console.log(gridData);

var grid = d3.select("#grid")
	.append("svg")
	.attr("width","510px")
	.attr("height","510px");
	
var row = grid.selectAll(".row")
	.data(gridData)
	.enter().append("g")
	.attr("class", "row");
	
var column = row.selectAll(".square")
	.data(d=>d)
	.enter().append("rect")
	.attr("class","square")
	.attr("x", d => d.x )
	.attr("y", d => d.y )
	.attr("width", d => d.width )
	.attr("height", d => d.height )
	.style("fill", d => getColour(d.click))
	.style("stroke", "#222")
	.on('click', function(d) {
       d.click ++;
	   d3.select(this).style("fill",getColour(d.click)); 
    });


function doRefresh() {
	gridData.forEach(function (row) {
		console.log(row[2].click);
	})
}

function loadSprintData() {
	var url="http://localhost:3000/team/Sid/2279/stories";
	//var url="../sid-stories.json";
	d3.json(url, function(error, dataset) {
		console.log(dataset);

		if (error) {
			throw error;
		}

		data = dataset;
		update();
	});
}


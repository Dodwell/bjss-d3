var svg = d3.select("svg"),
  margin = {
    top: 20,
    right: 20,
    bottom: 30,
    left: 40
  },
  width = +svg.attr("width") - margin.left - margin.right,
  height = +svg.attr("height") - margin.top - margin.bottom;
  
var color = d3.scaleOrdinal(d3.schemeCategory10);

var x = d3.scaleBand().rangeRound([0, width])
  .padding(0.1),
  y = d3.scaleLinear().rangeRound([height, 0]);

var g = svg.append("g")
  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
var data;
var dataset;

// the update function
const update = () => {


	var ymaxdomain = d3.max(data, function(d) {
	  return d.count;
	});
	x.domain(data.map(function(d) {
	  return d.months
	}));
	y.domain([0, ymaxdomain]);

	console.log("xb "+x.bandwidth());
	
	var x1 = d3.scaleOrdinal()
	  .domain(data.map(function(d) {
		return d.Group;
	  }));

	color.domain(data.map(function(d) {
	  return d.Group;
	}));

	var groups = g.selectAll(null)
	  .data(data)
	  .enter()
	  .append("g")
	  .attr("transform", function(d) {
		return "translate(" + x(d.months) + ",0)";
	  })

	var bars = groups.selectAll(null)
	  .data(function(d) {
		return [d]
	  })
	  .enter()
	  .append("rect")
	  .attr("x", function(d, i) {
		
		return x1(d.Group)
	  })
	  .attr("y", function(d) {
		return y(d.count);
	  })
	  .attr("width", 2)
	  .attr("height", function(d) {
		return height - y(d.count);
	  })
	  .attr("fill", function(d) {
		return color(d.Group)
	  })

	g.append("g")
	  .attr("class", "axis")
	  .attr("transform", "translate(0," + height + ")")
	  .call(d3.axisBottom(x));

	g.append("g")
	  .attr("class", "axis")
	  .call(d3.axisLeft(y).ticks(null, "s"))
	  .append("text")
	  .attr("x", 2)
	  .attr("y", y(y.ticks().pop()) + 0.5)
	  .attr("dy", "0.32em")
	  .attr("fill", "#000")
	  .attr("font-weight", "bold")
	  .attr("text-anchor", "start")
	  .text("count");
}


function doRefresh() {
	var url="./stories.json";
	//var url="../sid-stories.json";
	d3.json(url, function(error, dataset) {
		console.log(dataset);

		if (error) {
			throw error;
		}
		var storiesBySprint = d3.nest()
		   .key(function(d) { return d.doneSprintName; })
		   .entries(dataset);

		console.log(storiesBySprint);
		var newArray = [];
		var sprints = [];
		dataset.forEach(function (pbi) {
			
			var obj = {  "Key": pbi.key,
						  "Group": pbi.key,
						  "count": pbi.cycleTime,
						  "months": pbi.doneSprintName
			}
			newArray.push(obj);
		});
		
		data = newArray;
		console.log(data);
		console.log(data.map(d=> d.Group));
	 
		update();
	});
}

doRefresh();
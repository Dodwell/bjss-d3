var dataString = "Campaign,Click_Date,Start,End,Clicked,clickedFlag,Customer ID,weekDay,Age,Country,Demographic,Gender \nEXTORTION,30/12/2012,30/12/2012,29/01/2013,Clicked,1,10,Sun,30,UK,Adult,Male \nSALES,31/12/2012,30/12/2012,29/01/2013,Clicked,1,11,Mon,26,UK,Adult,Female \nSALES,01/01/2013,30/12/2012,29/01/2013,Clicked,1,12,Tue,59,UK,Adult,Male \nSALES,02/01/2013,30/12/2012,29/01/2013,Clicked,1,13,Wed,3,UK,Child,Male \nSALES,03/01/2013,30/12/2012,29/01/2013,Clicked,1,14,Thu,59,Germany,Adult,Female \nSALES,04/01/2013,30/12/2012,29/01/2013,No Click,0,15,Fri,39,UK,Adult,Male \nSALES,05/01/2013,30/12/2012,29/01/2013,Clicked,1,16,Sat,19,UK,Adult,Male \nSALES,07/01/2013,30/12/2012,29/01/2013,No Click,0,18,Mon,25,UK,Adult,Male \nSALES,08/01/2013,30/12/2012,29/01/2013,Clicked,1,19,Tue,6,UK,Child,Male \nSALES,09/01/2013,30/12/2012,29/01/2013,Clicked,1,20,Wed,55,Germany,Adult,Female \nSALES,10/01/2013,30/12/2012,29/01/2013,Clicked,1,21,Thu,19,UK,Adult,Male \nSALES,11/01/2013,30/12/2012,29/01/2013,No Click,0,22,Fri,32,UK,Adult,Male \nSALES,12/01/2013,30/12/2012,29/01/2013,Clicked,1,23,Sat,18,UK,Adult,Female \nSALES,14/01/2013,30/12/2012,29/01/2013,Clicked,1,25,Mon,7,UK,Child,Male \nSALES,30/12/2012,30/12/2012,29/01/2013,Clicked,1,32,Sun,59,France,Adult,Female \nSALES,31/12/2012,30/12/2012,29/01/2013,Clicked,1,33,Mon,28,France,Adult,Male \nSALES,01/01/2013,30/12/2012,29/01/2013,Clicked,1,34,Tue,31,UK,Adult,Male \nSALES,02/01/2013,30/12/2012,29/01/2013,No Click,0,35,Wed,3,France,Child,Female \nSALES,03/01/2013,30/12/2012,29/01/2013,Clicked,1,36,Thu,38,France,Adult,Male \nSALES,04/01/2013,30/12/2012,29/01/2013,Clicked,1,37,Fri,50,France,Adult,Male \nSALES,05/01/2013,30/12/2012,29/01/2013,Clicked,1,38,Sat,57,France,Adult,Female \nSALES,06/01/2013,30/12/2012,29/01/2013,Clicked,1,39,Sun,38,France,Adult,Male \nSALES,07/01/2013,30/12/2012,29/01/2013,Clicked,1,40,Mon,31,UK,Adult,Male \nSALES,08/01/2013,30/12/2012,29/01/2013,No Click,0,41,Tue,33,France,Adult,Female \nSALES,09/01/2013,30/12/2012,29/01/2013,Clicked,1,42,Wed,34,France,Adult,Male \nSALES,10/01/2013,30/12/2012,29/01/2013,Clicked,1,43,Thu,59,France,Adult,Male \nSALES,11/01/2013,30/12/2012,29/01/2013,No Click,0,44,Fri,13,France,Teen,Female \nSALES,12/01/2013,30/12/2012,29/01/2013,Clicked,1,45,Sat,2,France,Child,Male \nSALES,13/01/2013,30/12/2012,29/01/2013,Clicked,1,46,Sun,39,UK,Adult,Male \nSALES,30/12/2012,30/12/2012,29/01/2013,Clicked,1,54,Sun,18,France,Adult,Male \nSALES,01/01/2013,30/12/2012,29/01/2013,Clicked,1,55,Tue,13,France,Teen,Male \nSALES,11/01/2013,30/12/2012,29/01/2013,Clicked,1,56,Fri,50,France,Adult,Female \nSALES,11/01/2013,30/12/2012,29/01/2013,No Click,0,57,Fri,19,France,Adult,Male \nSALES,03/01/2013,30/12/2012,29/01/2013,Clicked,1,58,Thu,22,USA,Adult,Male \nSALES,04/01/2013,30/12/2012,29/01/2013,Clicked,1,59,Fri,11,USA,Child,Female \nSALES,05/01/2013,30/12/2012,29/01/2013,No Click,0,60,Sat,56,USA,Adult,Male \nSALES,11/01/2013,30/12/2012,29/01/2013,Clicked,1,61,Fri,7,USA,Child,Male \nSALES,07/01/2013,30/12/2012,29/01/2013,Clicked,1,62,Mon,9,USA,Child,Female \nSALES,08/01/2013,30/12/2012,29/01/2013,Clicked,1,63,Tue,43,France,Adult,Male \nSALES,09/01/2013,30/12/2012,29/01/2013,Clicked,1,64,Wed,2,France,Child,Male \nSALES,11/01/2013,30/12/2012,29/01/2013,Clicked,1,66,Fri,32,USA,Adult,Male \nSALES,12/01/2013,30/12/2012,29/01/2013,Clicked,1,67,Sat,4,USA,Child,Male \nSALES,13/01/2013,30/12/2012,29/01/2013,Clicked,1,68,Sun,47,USA,Adult,Female \nSALES,14/01/2013,30/12/2012,29/01/2013,No Click,0,69,Mon,49,USA,Adult,Male"
// set the dimensions and margins of the graph
var margin = {
    top: 20,
    right: 20,
    bottom: 30,
    left: 40
  },
  width = 300 - margin.left - margin.right,
  height = 200 - margin.top - margin.bottom;

// set the ranges
var x = d3.scaleBand()
  .range([0, width])
  .padding(0.1);
var y = d3.scaleLinear()
  .range([height, 0]);

// append the svg object to the body of the page
// append a 'group' element to 'svg'
// moves the 'group' element to the top left margin
var svg = d3.select("body").append("svg")
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g")
  .attr("transform",
    "translate(" + margin.left + "," + margin.top + ")");

// get the data 
var data = d3.csvParse(dataString);

//console.log(data[0]);

// format all data from strings
data.forEach(function(d) {
  d.data = +d.data;
});

// Array [ Object, Object ] Key: Clicked, Key: No Clicked
var nested_data = d3.nest()
  .key(function(d) {
    return d.Clicked;
  })
  .key(function(d) {
    return d.Campaign
  })
  .rollup(function(values) {
    return values.length;
  })
  .entries(data);

var clicked_data = nested_data.find(d => d.key == "Clicked").values

// Scale the range of the data in the domains
x.domain(clicked_data.map(function(d) {
  return d.key;
}));
y.domain([0, d3.max(clicked_data, function(d) {
  return d.value;
})]);

// append the rectangles for the bar chart
svg.selectAll(".bar")
  .data(clicked_data)
  .enter().append("rect")

  .attr("class", "bar")
  .attr("x", function(d) {
    return x(d.key);
  })
  .attr("width", x.bandwidth())

  .attr("y", function(d) {
    return y(d.value);
  })
  .attr("height", function(d) {
    return height - y(d.value);
  });



// add the x Axis
svg.append("g")
  .attr("transform", "translate(0," + height + ")")
  .call(d3.axisBottom(x));

// add the y Axis
svg.append("g")
  .call(d3.axisLeft(y));